//  5/7ソフトウェア開発実験 実習基礎データ

public class CheckNum {
    public static void main(String[] args) {
        Integer num;

        num = getNum(); // プロンプトを出して数字を入力させる
        System.out.println("偶数ですか？ >" + conv(isEven(num)));
        System.out.println("素数ですか？ >" + conv(isPrime(num)));
    }

    // 引数を「はい」「いいえ」に変換して返す
    public static String conv(Boolean f) {
        return "stub";
    }

    // 適当なプロンプトを出力し、数値を入力させる(その数値を返す)
    public static Integer getNum() {
        return 0;
    }

    // 渡された値が偶数かをチェックする(戻り値はBoolean)
    public static Boolean isEven(Integer n) {
        return false;
    }

    // 渡された値が素数かをチェックする(戻り値はBoolean)
    public static Boolean isPrime(Integer n) {
	for(int i = 2; i <= (n / 2); i++) {
		if((n % i) == 0){
			return false;
		}
	}
        return true;
    }
}
